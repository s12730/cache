package Singleton;

public class EnumValue 
{

	private int id;
	private int enumId;
	private String code;
	private String value;
	private String enumerationName;
	
public int getId()
	{
	return id;
	}
public void setid( int id) 
	{
	this.id=id;
	}

public int getEnumId()
{
return enumId;
}
public void setEnumId( int enumId) 
{
this.enumId=enumId;
}

public String  getCode()
{
return code;
}
public void setCode( String code) 
{
this.code=code;
}

public String getValue()
{
return value;
}
public void setValue(String value) 
{
this.value=value;
}

public String getEnumerationName()
{
return enumerationName;
}
public void setEnumerationName( String enumerationName) 
{
this.enumerationName=enumerationName;
}
public EnumValue(int id, int enumId, String code, String value, String enumerationName)
{
	this.id = id;
	this.enumId = enumId;
	this.code = code;
	this.value = value;
	this.enumerationName = enumerationName;
}

@Override
public String toString() 
{
    return "["+id+"] - "+enumId+" - "+code+"- "+value+" - "+ enumerationName;
}

}