package Singleton;
import java.util.HashMap;
import java.util.Map;
public class CacheUpdate implements Runnable 
{

	private Cache cache = null;
	private int refresh = 1000;
	private Map<String,  EnumValue> items;
	
	public CacheUpdate() 
	{
		items = new HashMap<String, EnumValue>();
		cache = Cache.getInstance();
	}

	public void getData(Map<String,  EnumValue> items) 
	{
		this.items = items;
	}
	
	public void update() 
	{
		cache.setItems(items);
	}
	
		
	@Override
	public synchronized void run() 
	{
		update(); 
		try {
			Thread.sleep(refresh);
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	
}