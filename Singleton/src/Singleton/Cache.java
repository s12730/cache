
package Singleton;
import java.util.HashMap;
import java.util.Map;

public class Cache 
{
	private Map<String,  EnumValue> items = new HashMap<String, EnumValue>();
	private static Cache instance;
	
	private Cache() {}	
	
	public static synchronized Cache getInstance() {
		if (instance == null) {	
			instance = new Cache();
		}
		return instance;	
    }
	
	public void clear() {
	    	   this.items.clear();
	    }

	public void setItems(Map<String, EnumValue> items) {
				this.items = items;
	}
		
	public Map<String, EnumValue> getItems() {
			return items;
	}
}