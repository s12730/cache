package Singleton;
import java.util.HashMap;
import java.util.Map;
public class Test {

	public static void main(String[] args) 
	{
				
				Cache cache = null;
				cache = Cache.getInstance();
				
				EnumValue value1 = new EnumValue(1, 1, "12730", "Magda", "Osoba");
				EnumValue value2 = new EnumValue(2, 2, "12567", "Iwo", "Chłopczyk");
				EnumValue value3 = new EnumValue(3, 3, "12456", "Dawid", "Chłopak");
				EnumValue value4 = new EnumValue(4, 4, "12333", "Marek", "Mężczyzna");
				
				Map<String, EnumValue> items = new HashMap<String, EnumValue>();
				
				items.put("Osoba", value1);
				items.put("Chłopczyk", value2);
				items.put("Chłopak", value3);
				items.put("Mężczyzna", value4);

				cache.setItems(items);
				
				System.out.println(cache.getItems());
				
	}
		
}
